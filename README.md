# Webprint CLI

This is a command-line client for the Aberdeen University Webprint service.
The tool enables you to run a simple command:

`webprint File.pdf`

This command sends the file to the printer, so you can simply use PullPrint on a campus printer to print your PDF.

See #Usage for more detailed examples.

## Installation

In order to use this tool, you need the Aberdeen Access Agent.
The Agent will keep you logged in without you having to type your password in all of the time.
`git clone https://gitlab.com/aucs/aaa`

After installing the Agent, you can simply put or link this to your binaries, and run it.

## Usage

Simplest form of usage (print a PDF, JPEG, PNG...)
`webprint CV.pdf`

Print in colour:
`webprint --colour ~/Football.jpeg`
`webprint --color ~/Soccer.jpeg`

Push to print queue instead of pushing directly to printer:
`webprint --queue CV.pdf`
`webprint --queue --color ~/Summer.jpeg`
(The last '--color' is ignored, as whilst the file is in the queue, it can still be printed in both mono / colour.)

Show the print queue:
`webprint --queue`
(You can simply run `webprint` to print this.)

Remove an item from the print queue:
`webprint --delete oops`
`webprint --delete Oops.pdf`
`webprint --delete pdf`
Please note that the last command will remove EVERY pdf file from your print queue.
In general, --delete removes every file whose name matches the expression.

Show the print log:
`webprint --log`
