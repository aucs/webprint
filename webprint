#!/usr/bin/env bash
# Print a file using webprint.abdn.ac.uk, from the command line.

url="https://webprint.abdn.ac.uk";

# Delete job by id.
delete_job(){
	page=$(get "$url/index.cfm?action=deletejob&jid=$1");
	if ! echo "$page" | grep -q "deleted successfully"; then
		echo "Error deleting print job! Sorry :(";
		exit 1;
	fi
}

# Print a job.
print_job(){
	job="$1";
	name=$(echo "$job" | awk -F: '{ print $1 }');
	jid=$(echo "$job" | awk -F: '{ print $2 }');
	pid=$(echo "$job" | awk -F: '{ print $3 }');
	query="-F JID=$jid -F PID=$pid -F NumberOfCopies=1 -F PageFrom=1 -F PageTo=999 -F Duplex=2 -F method=printjob";
	# --data-urlencode 
	page=$(post "$url/afunctions.cfm" "$query");
	if ! echo "$page" | grep -q "has been sent"; then
		echo "Error sending print job to the printer! Sorry :(";
		exit 1;
	fi
}

# Get details for all our jobs.
get_details(){
	echo "$1" | awk '/Colour-Pull/ { color=1 } /Mono-Pull/ { color=0 } /JID/ { gsub(/^value="|">$/,"",$4); JID=$4 } /PID/ { gsub(/^value="|">$/,"",$4); PID=$4 } /<td><a href="javascript/ { gsub(/^<.*)">|<\/a.*>$/,"",$0); name=$0 } /document contains color/ { print name":"JID":"PID":"color }';
}

files="";

remove="";
print="";
color=0; # Let's default to mono to save the environment.
fasttrack=1; # Print immediately, don't queue.
log=0; # Show log instead of queue.

while [ "$#" -gt 0 ]; do
	case $1 in
		--delete)
			# Remove a matching print.
			remove="$2 $remove";
			shift;
			;;
		--print)
			print="$2 $print";
			shift;
			;;
		--color|--colour)
			color=1;
			;;
		--queue)
			fasttrack=0;
			;;
		--log)
			log=1;
			;;
		--*)
			echo "Unknown command: $1";
			exit 1;
			;;
		*)
			file=$1;
			if ! [ -f "$file" ]; then
				echo "File '$file' does not exist!";
				exit 1;
			else
				files="$file $files";
			fi
			;;
	esac
	shift;
done

access --ok webprint || exit 1;
. $(access --lib);

# Logged in.
if ! [ -z "$files" ]; then
	for file in $files; do
		echo "Pushing $file to print queue...";

		printForm="-F type=file -F FileToPrint=@$file"

		page=$(post "$url/webprint.cfm" "$printForm");

		if [ "$fasttrack" = 1 ]; then
			file_name=$(basename "$file");
			print="$file_name $print";

			# Make sure it appears in the queue.
			printf "%s" "Waiting for $file_name to get propagated to the queue";
			while true; do
				printf ".";
				sleep 1;
				page=$(get "$url");
				details=$(get_details "$page"); # We don't want already printed ones.
				if echo "$details" | grep -q "$file_name"; then
					echo "Done!";
					break;
				fi
			done
		else
			# Sleep a while to not overload the server.
			sleep 0.2;
		fi
	done
	[ "$fasttrack" = 0 ] && exit;
fi

page=$(get "$url");

if [ "$log" = 1 ]; then
	printed=$(echo "$page" | awk '/<td>[^<> ]*<\/td>/ { gsub(/^<td>|<\/td>$/,"",$0); print $0 }');
	echo "Print log:";
	echo "$printed";
	exit;
fi

# If no arguments are given, show the print queue and printed items.
echo "Print queue:";

jobDetails=$(get_details "$page");

# Let's filter the right PIDs (printer IDs) depending on color.
jobDetails=$(echo "$jobDetails" | awk -F: '{ if ($4=="'$color'") print $0 }');

# IDs to remove from the queue.
removes="";
# IDs to print from the queue.
prints="";

# We want to convert multiples into OR statements.
remove=$(echo "$remove" | sed 's/\b \b/\\|/g');
print=$(echo "$print" | sed 's/\b \b/\\|/g');

for job in $jobDetails; do
	name=$(echo "$job" | awk -F: '{ print $1 }');
	if [ ! -z "$remove" ] && echo "$name" | grep -q $remove; then
		removes="$job $removes";
	elif [ ! -z "$print" ] && echo "$name" | grep -q $print; then
		prints="$job $prints";
	else
		echo "$name";
	fi
done

for job in $removes; do
	name=$(echo "$job" | awk -F: '{ print $1 }');
	id=$(echo "$job" | awk -F: '{ print $2 }');
	echo "Removing print job '$name'...";
	sleep 1;
	delete_job $id;
done

for job in $prints; do
	name=$(echo "$job" | awk -F: '{ print $1 }');
	id=$(echo "$job" | awk -F: '{ print $2 }');
	echo "Printing '$name'...";
	sleep 1;
	print_job $job;
done
